---
title: 'Auslastung der Mensa'
abstract: 'Diese Vorlage kann für die Erarbeitung der Projektdokumentation im Rahmen der Vorlesung "Modul 1.7 – IT-Management" im Wintersemester 2022/2023 an der Hochschule Kehl verwendet werden.'
author:
    - Markus Bischoff
    - Paulin Lang
    - Alice Lubberger
    - Philip Morgen
    - Michael Müller
date: \today

link-citations: true
toc: true
toc-title: Inhaltsverzeichnis
nocite: '@*'
titlepage: true
toc-own-page: true
titlepage-logo: "images/logo_HSKE.pdf"
logo-width: 130mm
---


# Einleitung und wofür Sensoren verwendet werden


In der modernen Welt sind Sensoren unverzichtbar geworden, da sie es ermöglichen, die realen Umstände in eine digitale Form zu übersetzen. Dies ermöglicht die Überwachung und Steuerung von Prozessen in Echtzeit und die Erfassung von Daten in großen Mengen. Sensoren kommen in einer Vielzahl von Anwendungen zum Einsatz, wie zum Beispiel in der Produktion, im Transportwesen, in der Medizintechnik, in der Energiegewinnung und vielen anderen Bereichen. Sie ermöglichen es, Prozesse automatisch zu steuern, Trends und Probleme frühzeitig zu erkennen.


Sensoren sind auch in der modernen Welt unerlässlich für die Automatisierung von Prozessen und die Optimierung von Abläufen. Sie ermöglichen es, Daten aus der Umwelt aufzunehmen und diese für die Erstellung von Prognosen und die Entwicklung von Prognosemodellen zu nutzen. Dies ermöglicht es Unternehmen, die Effizienz ihrer Prozesse zu verbessern und Kosten zu sparen.


In der modernen Welt spielt die Datenanalyse eine immer größere Rolle. Sensoren ermöglichen es, Daten in großen Mengen zu sammeln und diese dann auszuwerten, um Muster und Trends zu erkennen. Dies ermöglicht es Unternehmen, bessere Entscheidungen zu treffen und Prozesse besser zu optimieren.


Insgesamt sind Sensoren in der modernen Welt unverzichtbar geworden, da sie es ermöglichen, Prozesse zu automatisieren, Daten zu sammeln und auszuwerten und Entscheidungen auf der Grundlage von Daten zu treffen. Sie ermöglichen es Unternehmen, effizienter zu arbeiten und Kosten zu sparen und tragen so dazu bei, die Wettbewerbsfähigkeit zu erhöhen.
Einer von vielen Faktoren, durch die die voranschreitende Digitalisierung immer mehr profitieren kann, ist LORAWAN. Während vor nicht allzu langer Zeit viele Werte, wie Personenanzahlen, CO2 Werte oder Lichtwerte aufwändig ermittelt werden mussten, können nun dank drahtloser Systeme viele genaue Angaben in Sekundenschnelle gemacht werden. Im Rahmen unseres Forschungsprojekts im Fach IT-Management können wir dank selbst gelöteter Sensoren Daten ermitteln und diese dank passender Software weiterverarbeiten. Hierzu nutzen wir die Netzwerkarchitektur LORAWAN. Doch was genau ist LORAWAN? Dies wird in der folgenden Projektdokumentation unter anderem erklärt.
Für unser Projekt haben wir uns als Team überlegt, die Auslastung der Mensa mit Hilfe der Sensoren zu messen. Die Messungen sollen dann grafisch veranschaulicht dargestellt werden, um so eine Möglichkeit zu bieten, immer die momentane Auslastung in der Mensa abrufen zu können. 
Wie diese Daten ermittelt, weiterverarbeitet und präsentiert werden sollten, wird im Folgenden näher erläutert. Zudem gehen wir in diesem Bericht auf den Löt-Workshop und Herausforderungen ein, auf die wir im Projektverlauf gestoßen sind. Am Ende der Projektarbeit folgt ein Fazit und ein kurzer Ausblick in die Zukunft.


# Warum nun also die Mensa?


Bevor wir uns für die Auslastung der Mensa entschieden haben, haben wir verschiedene Möglichkeiten durchdacht, die sich für unser Projekt eignen würden. Wir kamen hierbei unter anderem auch auf die Messung der Belegung des Parkplatzes an der Hochschule. Bei weiterem Nachdenken fiel dann jedoch auf, dass die Überwachung des Parkplatzes sich als sehr schwierig herausstellt, da die Sensoren auch hier mit Strom versorgt werden müssten und die Sensoren, die uns zur Verfügung standen, lediglich messen würden, dass ein Auto den Sensor passiert, jedoch nicht, in welche Richtung das Auto fährt. Somit wäre zwar gemessen worden, wie viele Autos den Sensor am Tag passieren, jedoch nicht, wie die aktuelle Belegung des Parkplatzes aussieht, da es hierfür nötig wäre zu messen, wie viele Autos auf den Parkplatz fahren und wie viele Autos wieder von dem Parkplatz herunter fahren und dies dann ständig mit der Gesamtkapazität des Parkplatzes zu verrechnen. Demnach haben wir uns dann dafür entschieden, die Auslastung der Mensa zu messen.


Sensoren in einer Mensa können sinnvoll sein, um verschiedene Prozesse zu optimieren und zu automatisieren. Hier sind einige Anwendungsbereiche von Sensoren in einer Mensa:


Überwachung des Essensangebots: Sensoren können verwendet werden, um das Essensangebot in Echtzeit zu überwachen und Nachschubbedarf zu erkennen.
Steuerung der Kühlung und Beleuchtung: Sensoren können verwendet werden, um die Kühlung und Beleuchtung in der Mensa automatisch anzupassen, um Energie zu sparen.
Überwachung der Hygiene: Sensoren können verwendet werden, um die Hygienestandards in der Mensa zu überwachen und sicherzustellen, dass sie den geltenden Vorschriften entsprechen.
Überwachung des Kundenverkehrs: Sensoren können verwendet werden, um die Anzahl der Kunden in der Mensa zu überwachen und das Personalmanagement zu verbessern.


Dies sind nur einige Beispiele, wie Sensoren in einer Mensa sinnvoll eingesetzt werden können, um Prozesse zu optimieren und zu automatisieren.


Wir haben uns für letzteres entschieden.
Die Überwachung des Kundenverkehrs bedeutet, dass Sensoren verwendet werden, um die Anzahl der Personen zu erfassen, die die Mensa betreten und verlassen. Diese Informationen können verwendet werden, um das Personalmanagement in der Mensa zu verbessern.


Wenn z.B. bekannt ist, wie viele Kunden zu bestimmten Tageszeiten in der Mensa sind, kann das Personalmanagement die Anzahl der Bedienungen entsprechend anpassen, um lange Wartezeiten zu vermeiden und den Kundenservice zu verbessern. Ebenso kann es die Menge an Essen, das vorbereitet wird, anpassen, um Lebensmittelverschwendung zu vermeiden.


Insgesamt trägt die Überwachung des Kundenverkehrs zu einer effizienteren und angenehmeren Erfahrung für die Kunden bei und kann auch zu Kosteneinsparungen für das Personalmanagement führen.




# Was ist LoRaWAN 


LoRaWAN steht für Low Power Area Network und ist eine Spezifikation für drahtlos betriebene Systeme. LoRaWAN hat eine hohe Reichweite bei einem vergleichsweise niedrigen Stromverbrauch.  Dank einer sicheren Kommunikation können dank verschiedener Sensoren und Sendern viele Daten gesammelt werden. Anwendung findet das LoRaWAN besonders in der IoT (Internet of Things) und in vielen Smart Cities. So kann beispielsweise der Straßenverkehr oder die Parkplatzsituation genau überwacht werden. Pflanzen melden über die Sensoren, dass Sie Wasser brauchen oder Mülleimer wenn sie voll sind. In Deutschland gibt es bereits 140 Communities, die sich effektiv für den LoRaWAN Ausbau einsetzen und viele innovative Ideen anbieten, um Prozesse in Kommunen digital anbieten zu können. 


## Aufbau
Ein LoRaWa-Netz beruht auf Sensorik, die über Gateways mit Servern verbunden ist. Die Sensorik erfasst in unserem Bereich elektronische Geräte, wodurch sich die Anzahl der Geräte und somit die Anzahl an Personen beschreiben lässt. Durch die Erfassung der Daten ist es möglich, diese in Zahlen oder Grafiken darzustellen. Dabei dienen die LoRaWan-Gateways als Zwischenbrücke zwischen Sensoren und den Servern, wodurch eine Einsicht der Daten unabhängig vom Standort ermöglicht wird. 
Der Aufbau eines LoRaWAN- Netzes sieht wie folgt aus: Wie bereits erwähnt geht es hauptsächlich um die Anwendung von Sensorik, in unserem Beispiel PAX Counter, welche über Antennen, sogenannte Gateways, mit den Servern verbunden sind. Der Einsatz verschiedenster Sensoren erleichtert die öffentliche Arbeit in vielen Bereichen erheblich und genießt deshalb aktuell einen rasanten Anstieg in der Nutzerzahl (vgl. Dartmann und Bach 2020: 7). Über Sensorik, wie beispielsweise den Pax-Counter, lassen sich die Daten der Städte zusammentragen. Die Sensorik ist mit einem LoRaWAN Netz verbunden, beispielsweise dem TTN-Netz. Hierbei gelangen die erfassten Daten durch sogenannte LoRaWAN-Gateways auf die Netzwerkserver, wo die Daten weiterverarbeitet werden, bis sie dann beispielsweise im Browser abrufbar sind. Die Gateways sind also sozusagen die Vermittler zwischen den Sensoren und den Netzwerkservern und sind daher von immenser Bedeutung.


# Das Projekt Mensa Auslastung


Bei der Überlegung welches Szenario wir an der Hochschule überwachen möchten, sind wir recht zügig auf die Auslastung der Mensa gestoßen, da diese zu gewissen Zeiten sehr ausgelastet ist und wir den Studierenden hierfür die Auslastung in Form einer grafischen Darstellung anzeigen lassen möchten. Um die Personen in der Mensa erfassen zu können, haben wir uns für einen PAX Counter entschieden, der bereits in unserem Sensor enthalten ist. Den Sensor für die Überwachung haben wir in einem separaten Workshop selbständig zusammen gelötet, dazu später mehr. Mit dem PAX Counter können Geräte erkannt werden, die nach WLAN-Signalen suchen. Darüber können wir eine ungefähre Anzahl an Personen feststellen, die sich in der Mensa befinden. Gleichzeitig wird die Personenanzahl von uns gezählt, um einen Faktor herauszufinden, mit dem man dann berechnen kann, wie viele Personen sich tatsächlich in der Mensa befinden. Die Sensoren sind in silbernen Abzweigdosen und werden in der Mensa mit Hilfe von Powerbanks montiert. Auf einem Dashboard auf einer webbasierten Plattform - mhascaro, darauf wird später noch eingegangen - kann man nun die erfassten Daten mit passenden Grafiken oder Diagrammen anzeigen lassen. Unser Ziel ist es, die Auslastungsdaten auf der Hochschul-App anzeigen zu lassen, um die Kommilitonen*innen darüber zu informieren, zu welcher Zeit in der Mensa nicht viel Besuch ist.  


# Der Löt-Workshop


Am 24.11.22 fand in der Fahrzeughalle des Betriebshofs der Stadt Kehl ein Löt- Workshop statt. Während des Löt-Workshops sollten die benötigten Sensoren vorbereitet werden. Doch zuerst mussten die Grundlagen des Lötens gelernt werden. Der richtige Umgang mit Lötkolben und Lötzinn war essentiell, damit der Sensor letztendlich funktioniert.
Um dies zu gewährleisten, konnten wir, bevor wir an den eigentlichen Sensoren gearbeitet hatten, vorher noch an einem “Testkit” üben, um das genaue Löten zu erlernen.
An den benötigten Sensoren wurden dann die LoRaWan Chips an die Platine gelötet. Da wir für unser Projekt eine große Fläche abdecken müssen, haben wir gleich zwei Counter präpariert. Einen für die Mensa und einen für die Cafeteria.

S. Bild 1 und 2 Löt-Workshop


# Das mhascaro Portal 
Für die Auswertung ist es nötig, das mhascaro Portal zu nutzen. Um unsere gesammelten Werte nutzen zu können, benutzen wir verschiedene Charts auf einem Dashboard. Ein Chart zeigt die Anzahl der Personen, welche im Laufe des Tages gemessen wurden. Die Ergebnisse der Messung werden alle 75 Sekunden erfasst und in den Chart eingetragen. Neben diesem Chart wird ein weiterer mit der aktuellen Personenzahl dargestellt, welcher anzeigt, wie viele Personen sich aktuell in der Mensa aufhalten.
Die dadurch entstehende Kurve soll zeigen, wann sich besonders viele Personen in der Mensa aufhalten, und auch, ab wann die Mensa einen besonders hohen Andrang hat. Es können sich so tagesabhängig Graphen entwickeln lassen, um Trends zu verfolgen. Es kann gemessen werden, ob es an bestimmten Tagen besonders viele Studenten in der Mensa hat oder ob es mit bestimmten, besonders beliebten oder unbeliebten Gerichten zu tun hat.

S. Bild 3 Grafische Darstellung Daten


## Dashboard-Beschreibung 
Das Dashboard hat 2 Widgets. Das erste Widget zeigt die aktuelle Personenzahl in der Mensa in Form einer Kurve an. Diese Kurve zeigt den Verlauf sowie Hoch- und Tiefphasen und aktuelle Zahlen in der Kurve an. Damit können zukünftige Prognosen erstellt werden. 
Das zweite Widget zeigt die aktuelle Personenzahl in Form eines Tachometers an. Dadurch kann auf einen Blick gesehen werden, ob die Mensa hoch oder niedrig ausgelastet ist. Ab einem Wert von 130 kommt der Zähler in den roten Bereich, um zu signalisieren, dass die Mensa stärker ausgelastet ist.

S. Bild 4 Dashboard mhascaro Portal 


## Programmierung 
Die Programmierung wurde in Arduino vollzogen. Dafür wurde unter den Werkzeugen ArduBlock verwendet. Um regelmäßig Zahlen zu erfassen, mussten sämtliche Funktionen in einer Schleife programmiert werden. Eine Signalstärke von -95 war nötig, um den ungefähren Radius der Mensa abzudecken und gleichmäßige Ergebnisse während unserer Tests zu erzielen. Ebenso haben wir einen eher kurzen  Timeout gewählt, damit das Testen deutlich schneller und effizienter vollzogen wird. Sobald der Sensor in den Einsatz kommt, wird der Timeout länger sein, damit die Powerbank länger nicht aufgeladen werden muss. Aus denselben Gründen wird der Delay ebenfalls verlängert.
Sobald der Code auf den Oktopus hochgeladen wurde, konnte mit Hilfe von einem seriellen Monitor die Aktivitäten überwacht und nachvollzogen werden. Dadurch konnten die ersten Tests bereits vollzogen werden, ohne die mhascaro-Seite nutzen zu müssen.

S. Bild 5 Programmierung mhascaro Portal


# Herausforderungen 
Die erste Herausforderung stellte sich bereits in der Frage der Themenwahl. Dabei wichtig ist die Wirkung und die Relevanz des Themas und der Ergebnisse, welche erzielt werden. Mit der Wahl der Messungen der Mensa-Auslastung profitieren sowohl Studierende als auch Professoren der Hochschule Kehl. Sollten die Resultate in der Hochschul-App eingespielt werden, können die Betroffenen direkt einsehen, wie sehr die Mensa im aktuellen Moment ausgelastet ist.


Nachdem wir das Thema für die Hausarbeit ausgewählt hatten, haben wir uns mit verschiedenen potentiellen Herausforderungen auseinandergesetzt, welche im Laufe der Hausarbeit auftreten können. 
Häufig auftretende Probleme bei der Arbeit mit LoRaWAN-Sensoren sind vor allem die Reichweite, die Stromversorgung und die Störung durch andere Signale. 
Die Schwierigkeit der Reichweite spielt in unserem Fall keine Rolle, da unsere gebaute Sensorik in der Hochschule aufgebaut wird, weshalb sie in direkter Nähe des LoRaWAN-Gateways ist.
Das größte Problem beim Erarbeiten des Projekts, trat bei der Stromversorgung auf. Bei unserer Standortwahl in der Mensa und der Cafeteria der Hochschule Kehl kann keine durchgängige Stromversorgung durch eine Steckdose gewährleistet werden. Mit Hilfe von Powerbanks, welche mit den restlichen Mittel des Vorjahres finanziert wurden, war die Stromversorgung dennoch möglich. Aufgrund der Stromversorgung durch Powerbanks, haben wir unsere Programmierung in der Art angepasst, dass sich die Sensorik außerhalb der Öffnungszeiten der Mensa und der Cafeteria selbstständig ein- und ausschaltet. Dementsprechend kann der Stromverbrauch reduziert und die Kapazität der Powerbanks geschont werden.
Störungen der Signale waren in unserem Fall minimal. Bereits während den Vorlesungen haben wir Testmessungen durchgeführt und sind mit unseren Parametern auf zuverlässige Werte gestoßen. Da wir zwei Sensoren verwenden, ist es auch wichtig, die Schnittmenge auszugrenzen. Um dies zu ermöglichen, ist es naheliegend, die Ergebnisse der Messungen durch reale Zählungen zu vergleichen. Anhand dieser Werte haben wir die Feldstärke der Sensoren angepasst. Dabei spielt auch die Anzahl der elektronischen Geräte pro studierender Person eine Rolle, da so teilweise pro Person mehrere Geräte erfasst werden.


Aufgrund der Problematik der Stromversorgung hat sich der vorher festgelegte Zeitplan deutlich nach hinten verschoben. Da die Powerbanks zur Lösung nicht während der Vorlesungszeit angekommen sind, konnten wir keine realen Messungen in der Mensa und der Cafeteria durchführen und dementsprechend auch nicht auswerten.


# Fazit und Ausblick
Um es noch einmal zusammenzufassen, haben wir zuerst die Sensoren zusammengebaut und gelötet. Nach Fertigstellung der Sensoren wurden diese über Arduino programmiert. Nach der Programmierung waren die Sensoren bereit dafür, an ihren Standorten aufgehängt zu werden.
Sehr schade war natürlich, wie bereits oben erwähnt, dass es in unserem Fall aufgrund der fehlenden Powerbanks nicht möglich war, die Sensoren tatsächlich anzubringen. Deshalb konnten wir leider keine echten Daten von unseren Sensoren erhalten und diese auch nicht grafisch darstellen.
Dennoch empfanden wir das Projekt als sehr lehrreich auch für unsere berufliche Zukunft - schließlich verläuft auch in der Praxis nicht jedes Projekt genau nach seinem Zeitplan.
Zusammenfassend kann man sagen, dass es für uns als Team eine sehr interessante, lehrreiche Erfahrung war, die Sensoren so herzustellen und zu programmieren, dass sie einsatzfähig sind. Die Verbindung zwischen vorherigen theoretischen Vorlesungen und Hands-on-Workshop fanden wir sehr gut. 


In die Zukunft gesehen würden wir uns sehr freuen, wenn unser Projekt - vielleicht durch einen späteren DVM-Jahrgang - weitergeführt werden würde und die Daten so verwendet werden könnten, wie von uns geplant. Sobald echte Daten zur Verfügung stehen, die ausgewertet werden können, könnte auch der nächste Schritt angegangen werden, die Daten entsprechend grafisch aufgewertet in die Hochschul-App einzubinden.




![Löt-Workshop1.jpeg](./Löt-Workshop1.jpeg)
![Löt-Workshop2.jpeg](./Löt-Workshop2.jpeg)
![Grafische Darstellung Daten.jpeg](./Grafische Darstellung Daten.jpeg)
![Dashboard mhascaro Portal.jpeg](./Dashboard mhascaro Portal.jpeg)
![Programmirung mhascaro Portal.jpeg](./Programmirung mhascaro Portal.jpeg)

